# Como utilizar o Gitlab?
#### Seguem estas etapas para trabalharem com o Gitlab e versionarem os projetos

1 - Instalar o git no computador. Usuários do Ruindows instalarem o git bash 64 bits e Linux o 'sudo apt-get install git'

2 - Gerar uma chave SSH através do git bash com o comando 'ssh-keygen'

3 - Adicionar a chave ssh gerada pelo git bash no Gitlab. A chava gerado fica em 'Usuario/.ssh/id_rsa.pub'

4 - Clonar o repositório que está no Gitlab para a sua máquina. Para isso, basta utilizar o comando 'git clone git@gitlab.com:cimtech/no-do-repositorio.git'

5 - Adicionar qualquer alterações realizadas localmente com o comando 'git add .' ou 'git add all'

6 - Realizar o commit das alterações adcionadas anteriormente com o comando 'git commit -m "Mensagem descritiva". O ideal é que os commits sejam pequenos e constantes

7 - Até este ponto todas as alterações foram locais, para subir ela para o servidor é preciso solicitar o Merge Requests pela interface gráfica do Gitlab. O dono do repositório ira aprovar as alterações e assim o código no servidor estara atualizado

8 - Agora se um outro usuário fizer uma alteração no código do servidor, é preciso atualizar o código local de sua máquina. Para isso utilize o comando 'git pull origin master'
